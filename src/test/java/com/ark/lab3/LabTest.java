/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ark.lab3;

import static com.ark.lab3.Salaries.BONUS;
import static com.ark.lab3.Salaries.BONUS_MONTH;
import java.time.LocalDate;
import java.time.LocalTime;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author ahmed
 */
public class LabTest {

    private Employee employee;

    @Before
    public void init() {
        employee = getEmployee();
        Environment.currentDate = LocalDate.parse("2019-09-01");
    }

    @Test
    public void givenNormalCase_whenRequestSalary_thenWillReturned() {
        //When
        double salary = employee.getTotalSalary();
        //Then
        assertTrue(salary == 1000);
    }

    @Test
    public void givenInvalidRecieveDate_whenRequestSalary_thenWillReturned() {
        //Given
        employee.setRecieveDate(LocalDate.parse("2019-08-29"));
        //When
        double salary = employee.getTotalSalary();
        //Then
        assertTrue(salary == 0);

    }

    @Test
    public void givenInvalidWorkTime_whenRequestSalary_thenWillReturned() {
        //Given
        employee.setWorkTime(LocalTime.parse("08:30"));
        //When
        double salary = employee.getTotalSalary();
        //Then
        assertTrue(salary == 900);
    }

    @Test
    public void givenBonusMonth_whenRequestSalary_thenWillCalculated() {
        //Given
        employee.setBirthDate(LocalDate.parse("1995-04-01"));
        //When
        double salary=employee.getTotalSalary();
        //Then
        assertTrue(salary==1400);
    }

    //Utils
    private Employee getEmployee() {
        return new Employee(
                "Employee",
                LocalDate.parse("2019-09-01"),
                LocalDate.parse("1995-09-01"),
                LocalTime.parse("08:00"),
                1000
        );
    }
}
