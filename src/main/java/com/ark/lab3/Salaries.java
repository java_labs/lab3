/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ark.lab3;

import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author ahmed
 */
public enum Salaries {
    DISCOUNT(.1),WORK_TIME("08:00"),BONUS(.4),BONUS_MONTH(4);
    private  double value;
    private LocalTime _value;
    private Salaries(double value){
        this.value=value;
    }
    private Salaries(String value){
        this._value=LocalTime.parse(value);
    }
    
    public double getValue(){
        return value;
    }
    public LocalTime _getValue(){
        return _value;
    }
    
}
