/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ark.lab3;

import static com.ark.lab3.Salaries.DISCOUNT;
import static com.ark.lab3.Salaries.WORK_TIME;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 *
 * @author ahmed
 */
public class Employee {

    private String name;
    private LocalDate recieveDate;
    private LocalDate birthDate;
    private LocalTime workTime;
    private double salary;

    public Employee(String name, LocalDate recieveDate,
            LocalDate birthDate, LocalTime workTime, double salary) {
        this.name = name;
        this.recieveDate = recieveDate;
        this.birthDate = birthDate;
        this.workTime = workTime;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getRecieveDate() {
        return recieveDate;
    }

    public void setRecieveDate(LocalDate recieveDate) {
        this.recieveDate = recieveDate;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public LocalTime getWorkTime() {
        return workTime;
    }

    public void setWorkTime(LocalTime workTime) {
        this.workTime = workTime;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public double getTotalSalary() {
        if (recieveDate.isBefore(Environment.currentDate)) {
            return 0;
        }
        if (workTime.isAfter(WORK_TIME._getValue())) {
            return salary - salary * DISCOUNT.getValue();
        }
        return salary;
    }
}
