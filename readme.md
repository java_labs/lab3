#Description
##Employee
###attributes
1 - name
2 - recieveDate attribute in which the employee come and take his salary every month
3 - birthDate
4 - workTime attribute in which the employee must come to the job.
5 - salary attribute
###method
getTotalSalary method calculated as the following:
1 - If employee recieveDate is before the start of the month then salary will not be returned.
2 - If employee workTime is after the work time of the job then the salary will be decreased by .1 of the salary.
3 - If the month of the employee birthDate is eqault to 4 then bonus of .4 of salary will be added to the salary.
#Environment class
currentDate is the current date of the start of the month
#Requirements
Method getTotalSalary is handle the first 2 cases we mentioned above:
1 - If employee recieveDate is before the start of the month then salary will not be returned.
2 - If employee workTime is after the work time of the job then the salary will be decreased by .1 of the salary.

You are require to write code that meet the third case
3 - If the month of the employee birthDate is eqault to 4 then bonus of .4 of salary will be added to the salary.